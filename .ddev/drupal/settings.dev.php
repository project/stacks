<?php

$settings['container_yamls'][] = __DIR__ . '/development.services.yml';

// While we probably don't  need this, better to find it outside of
// drupal
$settings['config_sync_directory'] = '../config/sync';

$settings['skip_permissions_hardening'] = TRUE;

/**
 * Disable caches.
 *
 * @see example.settings.local.php
 */
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
