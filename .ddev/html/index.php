<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="dark:bg-slate-800">
  <div class="flex justify-center items-center">
    <div>
      <h1 class="mt-20 text-3xl font-bold text-white">
        Try out stacks!
      </h1>
      <a class="block text-white text-center text-xl bg-slate-500 w-full rounded mt-6 py-2 hover:bg-slate-200 hover:text-black transition-colors" href="<?php print str_replace('stacks', 'd9.stacks', $_ENV['DDEV_PRIMARY_URL']) ?>">Drupal 9</a>
      <a class="block text-white text-center text-xl bg-slate-500 w-full rounded mt-3 py-2 hover:bg-slate-200 hover:text-black transition-colors" href="<?php print str_replace('stacks', 'd10.stacks', $_ENV['DDEV_PRIMARY_URL']) ?>">Drupal 10</a>
      <div class="text-white text-center mt-6 border rounded w-fit p-4 mx-auto">
        <p><span class="text-slate-500">user:</span> admin</p>
        <p><span class="text-slate-500">pass:</span> admin</p>
      </div>
    </div>
  </div>
</body>
</html>
