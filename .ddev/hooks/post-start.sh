#!/bio/bash

set -e
set -x

drupal9_path=/var/drupal/d9
drupal10_path=/var/drupal/d10

function drupal_setup_common() {
  composer config --no-plugins allow-plugins.wikimedia/composer-merge-plugin true
  composer require wikimedia/composer-merge-plugin
  composer config extra.merge-plugin --merge --json '{"include": ["web/modules/contrib/chosen/composer.libraries.json"]}'
  composer config --no-plugins allow-plugins.oomphinc/composer-installers-extender true
  composer require oomphinc/composer-installers-extender
  composer require drush/drush drupal/admin_toolbar drupal/devel drupal/libraries drupal/chosen 'drupal/inline_entity_form:^2.0@RC' drupal/field_group drupal/jquery_ui_tabs
  # For now, install seven as stacks hasn't been properly tweaked for claro
  # We can install it on both d9 and d10
  # @see https://www.drupal.org/node/3304314
  composer require drupal/seven
  composer upgrade -W
  mkdir -p drush
  mkdir -p web/modules/custom
  ln -s /var/www/html web/modules/custom/stacks
  ln -s /var/www/html/.ddev/drupal/stacks_ddev web/modules/custom/stacks_ddev
  ln -fs /var/www/html/.ddev/drupal/settings.dev.php web/sites/default
  find web/modules/custom/stacks/ -mindepth 1 -type d -name stacks -exec cp -R {} web/core/themes/olivero ';'
}

function drupal_init_common() {
  if ! drush status --field=bootstrap | grep -q Successful; then
    drush site:install --account-name=admin --account-pass=admin -y
  fi
  drush en -y admin_toolbar_tools admin_toolbar_search devel
  # If we enable libraries with any of the other `drush en` calls, it fails
  # (gives an error on d10). Enabling it alone doesn't.
  drush en -y libraries
  drush en -y chosen, chosen_lib, inline_entity_form, field_group, jquery_ui_tabs, jquery_ui
  drush en -y stacks stacks_examples stacks_content_feed stacks_content_list stacks_example_code_grids
  # this module is inside .ddev, it only has some configs for a CT with astacks
  # field to be pre-configured
  drush en -y stacks_ddev
}

composer global config --no-plugins allow-plugins.dealerdirect/phpcodesniffer-composer-installer true
composer global require drupal/coder

mysql -uroot -proot -e "SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;"
mysql -uroot -proot -e 'CREATE DATABASE IF NOT EXISTS drupal9' mysql
mysql -uroot -proot -e 'CREATE DATABASE IF NOT EXISTS drupal10' mysql
mysql -uroot -proot -e "GRANT ALL ON drupal9.* to 'db'@'%' IDENTIFIED BY 'db';" mysql
mysql -uroot -proot -e "GRANT ALL ON drupal10.* to 'db'@'%' IDENTIFIED BY 'db';" mysql

sudo chown $(id -u):$(id -g) /var/drupal

if [[ ! -d $drupal9_path ]]; then
  composer create "drupal/recommended-project:^9" $drupal9_path
  cd $drupal9_path
  drupal_setup_common
else
  cd $drupal9_path
  composer upgrade -W
fi

if [[ ! -d $drupal10_path ]]; then
  composer create "drupal/recommended-project:^10" $drupal10_path
  cd $drupal10_path
  drupal_setup_common
else
  cd $drupal10_path
  composer upgrade -W
fi

cd /var/www/html
cp .ddev/drupal/settings9.php /var/drupal/d9/web/sites/default/settings.php
cp .ddev/drupal/settings9.ddev.php /var/drupal/d9/web/sites/default/settings.ddev.php
cp .ddev/drupal/settings10.php /var/drupal/d10/web/sites/default/settings.php
cp .ddev/drupal/settings10.ddev.php /var/drupal/d10/web/sites/default/settings.ddev.php

cd $drupal9_path
echo -e "options:\n  uri: '${DDEV_PRIMARY_URL/stacks/d9.stacks}'" > drush/drush.yml
drupal_init_common
# On d9, we use seven, on d10 we'll use claro, which is the default
drush then -y seven
drush cset -y system.theme admin seven
drush cr
drush updb -y

cd $drupal10_path
echo -e "options:\n  uri: '${DDEV_PRIMARY_URL/stacks/d10.stacks}'" > drush/drush.yml
drupal_init_common
drush cr
drush updb -y

cd $drupal9_path
drush uli
cd $drupal10_path
drush uli
