(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.stacks_finalize = {
    attach: function (context, settings) {
      var delta = drupalSettings.stacks.finalize.delta,
        widget_instance_id = drupalSettings.stacks.finalize.widget_instance_id,
        $completed_message = $('#completed_message');

      // Update the hidden input.
      $(once('stacks_finalize_update', '#widget-instance-' + delta + ' input')).val(widget_instance_id);

      // Trigger new row button.
      if ($completed_message.length > 0) {
        $(once('stacks_finalize_message', '#widget-form-' + delta)).each(function () {
          var $object = $(this);
          var parent = $object.closest('.field--type-stacks-type');
          $('.field-add-more-submit', parent).mousedown();
          $completed_message.html(Drupal.t('Loading...'));
        });

        $('#edit-actions').find('input').removeAttr('disabled').removeClass('is-disabled');
        $('a.remove-widget, a.edit-widget').show();
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
